<?php

// array_filter — Filters elements of an array using a callback function

// array array_filter ( array $array [, callable $callback )




$entry = array(
             0 => 'foo',
             1 => false,
             2 => -1,
             3 => null,
             4 => ''
          );

print_r(array_filter($entry));

