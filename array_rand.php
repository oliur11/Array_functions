<?php

// array_rand — Pick one or more random entries out of an array

// mixed array_rand ( array $array )




$input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
$rand_keys = array_rand($input, 2);
echo $input[$rand_keys[0]] . "<br>";
echo $input[$rand_keys[1]] . "<br>";

