<?php

// array_replace — Replaces elements from passed arrays into the first array

// array array_replace ( array $array1 , array $array2 )




$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(0 => "pineapple", 4 => "cherry");
$replacements2 = array(0 => "grape");

$basket = array_replace($base, $replacements, $replacements2);
print_r($basket);
