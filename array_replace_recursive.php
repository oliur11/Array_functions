<?php

// array_replace_recursive — Replaces elements from passed arrays into the first array recursively

// array array_replace_recursive ( array $array1 , array $array2 )




$base = array('citrus' => array( "orange") , 'berries' => array("blackberry", "raspberry"), );
$replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry'));

$basket = array_replace_recursive($base, $replacements);
print_r($basket);

