<?php

// array_walk — Apply a user supplied function to every member of an array

// bool array_walk ( array &$array , callable $callback




$fruits = array( "lemon", "orange", "banana", "apple");
array_walk($fruits, 'enumerate', &$num ); // reference here

print_r( $fruits );

