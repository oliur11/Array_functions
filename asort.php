<?php

// asort — Sort an array and maintain index association

// bool asort ( array &$array )




$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
asort($fruits);
foreach ($fruits as $key => $val) {
    echo "$key = $val";
    echo '<br>';
}

