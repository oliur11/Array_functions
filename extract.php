<?php

// extract — Import variables into the current symbol table from an array

// int extract ( array &$array)




$size = "large";
$var_array = array("color" => "blue","size"  => "medium","shape" => "sphere");

extract($var_array, EXTR_PREFIX_SAME, "wddx");

echo "$color, $size, $shape, $wddx_size";

?>